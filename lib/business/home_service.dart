import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:prueba_imag/data/home_entity.dart';

abstract class HomeService {
  Future<List<PopularMovies>> getPopularMovies();
}

class DefaultHomeService implements HomeService {
  @override
  Future<List<PopularMovies>> getPopularMovies() async {
    final url = Uri.parse(
      'https://api.themoviedb.org/3/movie/popular?api_key=36076b7426a866bc8a34e71e7be1dc04&language=en-US&page=1',
    );
    final response = await http.get(url);

    if (response.statusCode == 200) {
      final result = HomeEntity.fromJson(json.decode(response.body));

      return result.results;
    } else {
      throw Error();
    }
  }
}
