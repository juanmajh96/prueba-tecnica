import 'package:flutter/material.dart';

class ErrorWidgetUi extends StatelessWidget {
  const ErrorWidgetUi({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Icon(Icons.error_outline),
    );
  }
}
