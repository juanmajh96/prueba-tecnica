import 'package:flutter/foundation.dart';
import 'package:prueba_imag/data/home_entity.dart';

class ModelHomePage {
  final popularNavBarLabel = 'Popular Movies';
  final favoriteNavBarLabel = 'Favorite Movies';
  final appBarLabel = 'Movies';
  final currentIndex = ValueNotifier<int>(0);
  final isLoading = ValueNotifier<bool>(false);
  final error = ValueNotifier<bool>(false);
  final popularMovies = ValueNotifier<List<PopularMovies>>([]);
}
