import 'package:flutter/material.dart';
import 'package:prueba_imag/business/home_service.dart';
import 'package:prueba_imag/catalog/card.dart';
import 'package:prueba_imag/catalog/error_widget.dart';
import 'package:prueba_imag/catalog/loading_widget.dart';
import 'package:prueba_imag/catalog/notifiers.dart';
import 'package:prueba_imag/presenter/model.dart';
import 'package:prueba_imag/presenter/view_model.dart';

class HomePageInyection extends StatelessWidget {
  const HomePageInyection({super.key});

  @override
  Widget build(BuildContext context) {
    return HomePageInheritedWidget(
      viewModel: ViewModelHomePage(
        ModelHomePage(),
        DefaultHomeService(),
      )..loadData(),
      child: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final PageController controller;
  @override
  void initState() {
    super.initState();
    controller = PageController(initialPage: 0);
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = HomePageInheritedWidget.of(context).viewModel;
    return Scaffold(
      appBar: AppBar(title: Text(viewModel.model.appBarLabel)),
      body: PageView(
        controller: controller,
        children: const <Widget>[
          PopularMovies(),
          FavoriteMovies(),
        ],
      ),
      bottomNavigationBar: ValueListenableBuilder(
        valueListenable: viewModel.model.currentIndex,
        builder: (context, currentIndex, child) => BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: const Icon(Icons.people_outline_sharp),
              label: viewModel.model.popularNavBarLabel,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.favorite),
              label: viewModel.model.favoriteNavBarLabel,
            )
          ],
          currentIndex: currentIndex,
          onTap: (int value) {
            viewModel.onTapbottonNavigation(value);
            controller.jumpToPage(value);
          },
        ),
      ),
    );
  }
}

class PopularMovies extends StatelessWidget {
  const PopularMovies({super.key});

  @override
  Widget build(BuildContext context) {
    final viewModel = HomePageInheritedWidget.of(context).viewModel;
    return MultipleValueListenableBuilder(
      first: viewModel.model.popularMovies,
      second: viewModel.model.error,
      third: viewModel.model.isLoading,
      builder: (context, popularMovies, error, isLoading, child) =>
          isLoading == true
              ? const LoadingWidget()
              : error == false
                  ? ListView.builder(
                      physics: const BouncingScrollPhysics(),
                      itemCount: popularMovies.length,
                      itemBuilder: (context, index) => CardWidget(
                        popularMovies: popularMovies[index],
                      ),
                    )
                  : const ErrorWidgetUi(),
    );
  }
}

class FavoriteMovies extends StatelessWidget {
  const FavoriteMovies({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
    );
  }
}

class HomePageInheritedWidget extends InheritedWidget {
  const HomePageInheritedWidget({
    super.key,
    required Widget child,
    required this.viewModel,
  }) : super(child: child);

  final ViewModelHomePage viewModel;

  static HomePageInheritedWidget of(BuildContext context) {
    final HomePageInheritedWidget? result =
        context.dependOnInheritedWidgetOfExactType<HomePageInheritedWidget>();
    assert(result != null, 'No  found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(covariant HomePageInheritedWidget oldWidget) =>
      viewModel != oldWidget.viewModel;
}
