import 'package:prueba_imag/business/home_service.dart';
import 'package:prueba_imag/data/home_entity.dart';
import 'package:prueba_imag/presenter/model.dart';

class ViewModelHomePage {
  ViewModelHomePage(this.model, this.homeService);
  final ModelHomePage model;
  final HomeService homeService;

  onTapbottonNavigation(int currentIndex) =>
      model.currentIndex.value = currentIndex;

  void loadData() {
    model.isLoading.value = true;
    homeService
        .getPopularMovies()
        .then(_handleResponse)
        .onError((error, _) => _showError())
        .whenComplete(_finally);
  }

  void _handleResponse(List<PopularMovies> homeResponse) =>
      model.popularMovies.value = homeResponse;

  void _showError() => model.error.value = true;

  void _finally() {
    model.isLoading.value = false;
  }
}
